//
//  LoadingView.m
//  PrivaliaCaseObjC
//
//  Created by alexei on 4/24/18.
//  Copyright © 2018 privalia. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self = [[[NSBundle mainBundle] loadNibNamed:@"LoadingView"
                                              owner:self options:nil]
                objectAtIndex:0];
        self.frame = frame;
    }
    return self;
}

-(void)Reset{
    self.circle1.alpha  = 0.33;
    self.circle2.alpha  = 0.33;
    self.circle3.alpha  = 0.33;
    self.circle1.transform = CGAffineTransformMakeScale(1, 1);
    self.circle2.transform = CGAffineTransformMakeScale(1, 1);
    self.circle3.transform = CGAffineTransformMakeScale(1, 1);
}
-(void)animate:(BOOL)flag{
    
    [UIView animateWithDuration:0.4 animations:^{
        if (flag) {
            self.circle2.transform = CGAffineTransformMakeScale(0.3, 0.3);
            self.circle1.transform = CGAffineTransformMakeScale(1, 1);
            self.circle3.transform = CGAffineTransformMakeScale(1, 1);
            self.circle2.alpha = 0.33;
            self.circle1.alpha = 1;
            self.circle3.alpha = 1;
        }
        else
        {
            self.circle2.transform = CGAffineTransformMakeScale(1, 1);
            self.circle1.transform = CGAffineTransformMakeScale(0.3, 0.3);
            self.circle3.transform = CGAffineTransformMakeScale(0.3, 0.3);
            
            self.circle2.alpha = 1;
            self.circle1.alpha = 0.33;
            self.circle3.alpha = 0.33;
        }
    } completion:^(BOOL finished) {
        if (self.shouldAnimate) {
            [self animate:!flag];
        }
    }];
  
}
-(void)startAnimate{
    self.shouldAnimate = YES;
    [self animate:YES];
}
-(void)stopAnimate{
     self.shouldAnimate = NO;
}

@end
