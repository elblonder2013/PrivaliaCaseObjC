//
//  CellFilm.h
//  PrivaliaCaseObjC
//
//  Created by alexei on 4/24/18.
//  Copyright © 2018 privalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellFilm : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *overviewLabel;
@property (weak, nonatomic) IBOutlet UIImageView *movieImageView;  
@end
