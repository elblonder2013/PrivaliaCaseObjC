//
//  LoadingView.h
//  PrivaliaCaseObjC
//
//  Created by alexei on 4/24/18.
//  Copyright © 2018 privalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (weak, nonatomic) IBOutlet UIView *circle1;
@property (weak, nonatomic) IBOutlet UIView *circle2;
@property (weak, nonatomic) IBOutlet UIView *circle3;
@property (nonatomic)  BOOL shouldAnimate;
-(void)Reset;
-(void)animate:(BOOL)flag;
-(void)startAnimate;
-(void)stopAnimate;
@end
