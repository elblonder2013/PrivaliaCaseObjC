//
//  SearchMovesViewController.m
//  PrivaliaCaseObjC
//
//  Created by alexei on 4/24/18.
//  Copyright © 2018 privalia. All rights reserved.
//

#import "SearchMovesViewController.h"
#import "DetailsMovieViewController.h"
#import "LoadingView.h"
#import <UNIRest.h>
#import "CellFilm.h"
#import "SearchMovesViewController.h"
#import "UIImageView+WebCache.h"
@interface SearchMovesViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    
    BOOL searching;
    CGFloat keyBoardHeight;
    NSInteger searchPage;
    NSMutableArray * arrayFilms;
    NSInteger pullRefreshValue;
    LoadingView *loadingView;
    UNIUrlConnection *asyncConnection;
}

@property (weak, nonatomic) IBOutlet UIView *topBarView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *searchingIndicator;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITableView *resultTaleView;
@property (weak, nonatomic) IBOutlet UIView *searchResultView;
@property (weak, nonatomic) IBOutlet UIImageView *searchResultImage;
@property (weak, nonatomic) IBOutlet UILabel *searchResultLabel;

@end

@implementation SearchMovesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //init values
    searching = NO;
    keyBoardHeight = 0.0;
    searchPage = 1;
    arrayFilms = [[NSMutableArray alloc] init];
    pullRefreshValue = 100;
    
    CGRect rect = self.topBarView.frame;
    rect.origin.y -= self.topBarView.frame.size.height;
    self.topBarView.frame =rect;
    
    self.searchTextField.delegate = self;
    self.resultTaleView.delegate = self;
    self.resultTaleView.dataSource = self;
    self.resultTaleView.rowHeight = UITableViewAutomaticDimension;
    self.resultTaleView.estimatedRowHeight = 151;
    self.searchingIndicator.hidden = YES;
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    self.resultTaleView.tableFooterView = footerView;
    loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(footerView.frame.size.width/2-15, 0, 30, 30)];
    [footerView addSubview:loadingView];
    loadingView.hidden = YES;
    

}
-(void)viewDidAppear:(BOOL)animated
{
    if (keyBoardHeight == 0.0) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [_searchTextField becomeFirstResponder];
    }
    if (self.topBarView.frame.origin.y < 0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect rect = self.topBarView.frame;
            rect.origin.y = 0;
            self.topBarView.frame = rect;
        }];
       
    }
}

- (void)keyboardWillShow:(NSNotification *) notification{
    if (keyBoardHeight<=0) {
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        if (keyBoardHeight == 0.0)
        {
            CGFloat keyboardHeightGet = keyboardSize.height;
            keyBoardHeight = keyboardHeightGet;
            CGRect rect = self.resultTaleView.frame;
            rect.size.height = rect.size.height - keyBoardHeight+1;
            self.resultTaleView.frame = rect;
        }
        
    }
    
}

-(void)findMoviesFromApiRest:(BOOL)fromSearch  queryString:(NSString *)queryString
{
    NSDictionary *headers = @{@"accept": @"application/json",
                              @"trakt-api-version": @"2",
                              @"trakt-api-key": @"019a13b1881ae971f91295efc7fdecfa48b32c2a69fe6dd03180ff59289452b8"
                              };
    searching = true;
    self.searchingIndicator.hidden = NO;
    [self.searchingIndicator startAnimating];
    NSInteger pageAux= searchPage;
    if (!fromSearch) {
        pageAux += 1;
    }
    
    //we need to force get only 10 movies by request, so the param limit take 10 value.    
    NSString * url = [NSString stringWithFormat:@"https://api.trakt.tv/movies/popular?extended=full&limit=%@&page=%li&query=%@",@10,(long)pageAux,queryString];
    asyncConnection =  [[UNIRest get:^(UNISimpleRequest *request) {
        [request setUrl:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        [request setHeaders:headers];
    }] asJsonAsync:^(UNIHTTPJsonResponse* response, NSError *error) {
        searching = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.searchingIndicator.hidden = YES;
            if (error==nil) {
                
                NSInteger code = response.code;
                if (code==200) {
                    NSData *rawBody = response.rawBody;
                    NSError *jsonError;
                    NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:rawBody options:NSJSONReadingMutableContainers error:&jsonError];
                    
                    if (jsonError==nil) {
                        NSArray * array = (NSArray *)jsondata; //convert the dictionary to array
                        if (fromSearch) {
                            arrayFilms = [NSMutableArray arrayWithArray:array];
                            searchPage = 1;
                        }
                        else
                        {
                            
                            if (array.count>0){
                                [arrayFilms addObjectsFromArray:array];
                                searchPage = pageAux;
                            }
                        }
                        if (arrayFilms.count>0) {
                            self.resultTaleView.hidden = NO;
                            self.searchResultView.hidden = YES;
                        }
                        else {
                            self.resultTaleView.hidden = true;
                            self.searchResultView.hidden = false;
                            self.searchResultLabel.text = @"Your search don't have results";
                            self.searchResultImage.image = [UIImage imageNamed:@"search_status_not_found"];
                        }
                        [self.resultTaleView reloadData];
                        [loadingView stopAnimate];
                        [loadingView Reset];
                        loadingView.hidden = YES;
                    }
                    else//some error in response
                    {
                        self.resultTaleView.hidden = YES;
                        self.searchResultView.hidden = NO;
                        self.searchResultLabel.text = @"Upss... something was wrong";
                        self.searchResultImage.image = [UIImage imageNamed:@"search_status_error"];
                    }
                }
            }
            else
            {
                self.resultTaleView.hidden = YES;
                self.searchResultView.hidden = NO;
                self.searchResultLabel.text = @"Upss... something was wrong";
                self.searchResultImage.image = [UIImage imageNamed:@"search_status_error"];
            }
        });
      
    }];
}


-(IBAction)Close:(UIButton *)sender
{
    [self stopAllRequests];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = sender.superview.frame;
        rect.origin.y = sender.superview.frame.origin.y-sender.superview.frame.size.height-20;
        sender.superview.frame = rect;
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.searchTextField resignFirstResponder];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
  
}

-(void)stopAllRequests{
    
    if (asyncConnection!=nil) {
        [asyncConnection cancel];
    }
}




#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFilms.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellFilmID";
    
    CellFilm *cell =  [tableView
                       dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CellFilm alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSDictionary * dictFilm = arrayFilms[indexPath.row];
    if (dictFilm[@"title"] != nil && [dictFilm[@"title"] class] != [NSNull class])
    {
        cell.titleLabel.text = dictFilm[@"title"];
    }
    if (dictFilm[@"overview"] != nil && [dictFilm[@"overview"] class] != [NSNull class])
    {
        cell.overviewLabel.text = dictFilm[@"overview"];
    }
    if (dictFilm[@"year"] != nil && [dictFilm[@"year"] class] != [NSNull class])
    {
        cell.yearLabel.text = [NSString stringWithFormat:@"%i",[dictFilm[@"year"] intValue]];
    }
    if (dictFilm[@"ids"][@"imdb"]!=nil) {
        NSString * urlForPoster = [NSString stringWithFormat:@"http://img.omdbapi.com/?apikey=985eef78&i=%@", dictFilm[@"ids"][@"imdb"]];
        [cell.movieImageView sd_setImageWithURL:[NSURL URLWithString:urlForPoster] placeholderImage:[UIImage imageNamed:@"lauch_image"]];
    }
    else
    {
        cell.movieImageView.image = [UIImage imageNamed:@"lauch_image"];
    }
    
    return cell;
}

#pragma mark - TABLEVIEW DELEGATE

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailsMovieViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DETAILSUI"];
    vc.movieDetails = arrayFilms[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

//delegate used to make pull resfresh at bootom
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (searching) {
        return;
    }
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
    {
        CGFloat difference = (scrollView.contentOffset.y + scrollView.frame.size.height)-scrollView.contentSize.height;
        
        if (difference > 0 && !(loadingView.shouldAnimate))
        {
            [loadingView Reset];
            loadingView.hidden = NO;
            if (difference <= 33)
            {
                loadingView.circle1.alpha = 1;
            }
            else if (difference > 33 && difference <= 66)
            {
                loadingView.circle1.alpha = 1;
                loadingView.circle2.alpha = 1;
            }
            else
            {
                loadingView.circle1.alpha = 1;
                loadingView.circle2.alpha = 1;
                loadingView.circle3.alpha = 1;
                [loadingView startAnimate];
                [self findMoviesFromApiRest:NO queryString:self.searchTextField.text];
                
            }
            
        }
        else
        {
            loadingView.hidden = YES;
        }
        
    }
}

#pragma mark - SEARCHTEXTFIELD DELEGATE
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
   
    if(searching)
    {
        [self stopAllRequests];
        searching = NO;
    }
    searchPage = 1;
    [self findMoviesFromApiRest:YES queryString:newString];
     return YES;
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (keyBoardHeight>0.0) {
      CGRect rect =  self.resultTaleView.frame;
      rect.size.height = self.view.frame.size.height-self.resultTaleView.frame.origin.y-keyBoardHeight+1;
      self.resultTaleView.frame =rect;
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    CGRect rect =  self.resultTaleView.frame;
    rect.size.height = self.resultTaleView.frame.size.height+keyBoardHeight-1;
    self.resultTaleView.frame = rect;
    return YES;
}

@end
