//
//  MoviesListViewController.m
//  PrivaliaCaseObjC
//
//  Created by alexei on 4/24/18.
//  Copyright © 2018 privalia. All rights reserved.
//

#import "MoviesListViewController.h"
#import "LoadingView.h"
#import <UNIRest.h>
#import "CellFilm.h"
#import "SearchMovesViewController.h"
#import "UIImageView+WebCache.h"
#import "DetailsMovieViewController.h"
@interface MoviesListViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray * arrayFilms;
    NSInteger page;
    BOOL loading;
    LoadingView * loadingView;
    BOOL dataWasLoaded;
    UNIUrlConnection *asyncConnection;
}
@property (weak, nonatomic) IBOutlet UITableView *tableViewFilms;
@property (weak, nonatomic) IBOutlet UIButton *tryAgainBtn;
@property (weak, nonatomic) IBOutlet UIView *loadResultView;

@end

@implementation MoviesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayFilms = [[NSMutableArray alloc] init];
    page = 1;
    loading = dataWasLoaded = NO;
    self.tryAgainBtn.layer.borderColor = [UIColor blackColor].CGColor;
    self.tableViewFilms.delegate = self;
    self.tableViewFilms.dataSource = self;
    self.tableViewFilms.rowHeight = UITableViewAutomaticDimension;
    self.tableViewFilms.estimatedRowHeight = 151;
    
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    self.tableViewFilms.tableFooterView = footerView;
    loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-15, self.view.frame.size.height/2-15, 30, 30)];
    [self.view addSubview:loadingView];
    [loadingView startAnimate];
    
}

-(void)viewDidAppear:(BOOL)animated{
    if(!dataWasLoaded) {
        [self getMoviesFromApiRest];
    }
}

-(void)getMoviesFromApiRest{
    NSDictionary *headers = @{@"content-type": @"application/json",
                              @"trakt-api-version": @"2",
                              @"trakt-api-key": @"019a13b1881ae971f91295efc7fdecfa48b32c2a69fe6dd03180ff59289452b8"
                              };
    loading = true;
    NSString * url = [NSString stringWithFormat:@"https://api.trakt.tv/movies/popular?page=%li&extended=full&limit=%@",(long)page,@10];
    
    
   asyncConnection =  [[UNIRest get:^(UNISimpleRequest *request) {
        [request setUrl:url];
        [request setHeaders:headers];
       //[request setParameters:parameters];
    }] asJsonAsync:^(UNIHTTPJsonResponse* response, NSError *error) {
        
        loading = false;
           dispatch_async(dispatch_get_main_queue(), ^{
               [loadingView stopAnimate];
               [loadingView Reset];
               loadingView.hidden = YES;
           });
       

        
        if (error==nil) {
            NSInteger code = response.code;
            if (code==200) {
                NSData *rawBody = response.rawBody;
                NSError *jsonError;
                NSMutableDictionary *jsondata = [NSJSONSerialization JSONObjectWithData:rawBody options:NSJSONReadingMutableContainers error:&jsonError];
              
                if (jsonError==nil) {
                    NSArray * array = (NSArray *)jsondata; //convert the dictionary to array
                    if (!dataWasLoaded) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              self.tableViewFilms.hidden = NO;
                              self.loadResultView.hidden = YES;
                              dataWasLoaded = YES;
                              [self.tableViewFilms.tableFooterView addSubview:loadingView];
                              loadingView.frame = CGRectMake(self.tableViewFilms.tableFooterView.frame.size.width/2-15, 0, 30, 30);
                          });
                        
                       
                    }
                    if (array.count>0){
                        page+=1;
                        [arrayFilms addObjectsFromArray:array];
                        dispatch_async(dispatch_get_main_queue(), ^{
                           [self.tableViewFilms reloadData];
                       });
                    }
                }
                else
                {
                   
                }
            }
        }
        else
        {
            if (!dataWasLoaded)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.tableViewFilms.hidden = YES;
                    self.loadResultView.hidden = NO;
                });
            }
            else
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"An error ocurred loading the movies" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    loadingView.hidden = NO;
                    [loadingView startAnimate];                   
                    [self getMoviesFromApiRest];
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [loadingView stopAnimate];
                    [loadingView Reset];
                    loadingView.hidden = YES;
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }];
    
    
}

- (IBAction)TryAgain:(id)sender {
    loadingView.hidden = NO;
    self.loadResultView.hidden = YES;
    [loadingView startAnimate];
    [self getMoviesFromApiRest];
}
- (IBAction)SearchMoview:(id)sender {
    SearchMovesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SEARCHUI"];
    vc.view.backgroundColor = [UIColor colorWithRed:50/255. green:50/255. blue:50/255. alpha:1.0];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [navigationController setNavigationBarHidden:YES];		
    navigationController.view.backgroundColor = [UIColor clearColor];
    navigationController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:navigationController animated:NO completion:nil];
}


#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFilms.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"CellFilmID";
    
    CellFilm *cell =  [tableView
                       dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CellFilm alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSDictionary * dictFilm = arrayFilms[indexPath.row];
    cell.titleLabel.text = dictFilm[@"title"];
    cell.overviewLabel.text = dictFilm[@"overview"];
    cell.yearLabel.text = [NSString stringWithFormat:@"%i",[dictFilm[@"year"] intValue]];
    if (dictFilm[@"ids"][@"imdb"]!=nil) {
        NSString * urlForPoster = [NSString stringWithFormat:@"http://img.omdbapi.com/?apikey=985eef78&i=%@", dictFilm[@"ids"][@"imdb"]];
        [cell.movieImageView sd_setImageWithURL:[NSURL URLWithString:urlForPoster] placeholderImage:[UIImage imageNamed:@"lauch_image"]];
    }
    else
    {
        cell.movieImageView.image = [UIImage imageNamed:@"lauch_image"];
    }
    return cell;
}
#pragma mark - TABLEVIEW DELEGATE
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailsMovieViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DETAILSUI"];
    vc.movieDetails = arrayFilms[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (loading) {
        return;
    }
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
    {
        CGFloat difference = (scrollView.contentOffset.y + scrollView.frame.size.height)-scrollView.contentSize.height;
        
        if (difference > 0 && !(loadingView.shouldAnimate))
        {
            [loadingView Reset];
            loadingView.hidden = NO;
            if (difference <= 33)
            {
                loadingView.circle1.alpha = 1;
            }
            else if (difference > 33 && difference <= 66)
            {
                loadingView.circle1.alpha = 1;
                loadingView.circle2.alpha = 1;
            }
            else
            {
                loadingView.circle1.alpha = 1;
                loadingView.circle2.alpha = 1;
                loadingView.circle3.alpha = 1;
                [loadingView startAnimate];
                [self getMoviesFromApiRest];
                
            }
            
        }
        else
        {
            loadingView.hidden = YES;
        }
        
    }
}


@end
