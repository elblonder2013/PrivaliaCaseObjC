//
//  DetailsMovieViewController.m
//  PrivaliaCaseObjC
//
//  Created by alexei on 4/24/18.
//  Copyright © 2018 privalia. All rights reserved.
//

#import "DetailsMovieViewController.h"
#import "UIImageView+WebCache.h"
@interface DetailsMovieViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *containerScroll;
@property (weak, nonatomic) IBOutlet UILabel *genersLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *overViewLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;


@property (weak, nonatomic) IBOutlet UIImageView *start1Image;
@property (weak, nonatomic) IBOutlet UIImageView *start2Image;
@property (weak, nonatomic) IBOutlet UIImageView *start3Image;
@property (weak, nonatomic) IBOutlet UIImageView *start4Image;
@property (weak, nonatomic) IBOutlet UIImageView *start5Image;

@end

@implementation DetailsMovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ((self.movieDetails[@"trailer"] != nil) && [self.movieDetails[@"trailer"] class] != [NSNull null])
    {
        self.playButton.hidden = NO;
    }
    
    self.titleLabel.text = self.movieDetails[@"title"];
    self.overViewLabel.text = self.movieDetails[@"overview"];
    self.reviewLabel.text = [NSString stringWithFormat:@"%.1f",[self.movieDetails[@"rating"] floatValue]];
    
    //Showing the stars rating
    NSArray * arrayStart = @[self.start1Image,self.start2Image,self.start3Image,self.start4Image,self.start5Image];
    NSInteger ratingInt = [self.movieDetails[@"rating"] integerValue]/2;
    for (NSInteger i=1; i<=ratingInt; i++) {
        [arrayStart[i-1] setImage:[UIImage imageNamed:@"start_black"]];
    }
    NSArray * arrgenres = self.movieDetails[@"genres"];
    if (arrgenres.count>0)
    {
        self.genersLabel.text = arrgenres[0];
        if (arrgenres.count>1)
        {
            for (NSInteger i=1; i<=arrgenres.count-1; i++) {
                self.genersLabel.text = [NSString stringWithFormat:@"%@, %@",self.genersLabel.text,arrgenres[i]];
            }          
        }
    }
    if (self.movieDetails[@"ids"][@"imdb"]!=nil) {
        NSString * urlForPoster = [NSString stringWithFormat:@"http://img.omdbapi.com/?apikey=985eef78&i=%@", self.movieDetails[@"ids"][@"imdb"]];
        [self.posterImageView sd_setImageWithURL:[NSURL URLWithString:urlForPoster] placeholderImage:[UIImage imageNamed:@"lauch_image"]];
        [self.bannerImageView sd_setImageWithURL:[NSURL URLWithString:urlForPoster] placeholderImage:[UIImage imageNamed:@"lauch_image"]];
    }
   
}
-(IBAction)PlayTrailer:(id)sender {
    NSString * trailerUrl = self.movieDetails[@"trailer"];
    
    if ([trailerUrl containsString:@"youtube"])
    {
        NSArray * components = [trailerUrl componentsSeparatedByString:@"="];
        NSString * youtubeId = components.lastObject;
        NSURL *linkToApp = [NSURL URLWithString:[NSString stringWithFormat:@"youtube://watch?v=%@",youtubeId]];
        NSURL *linkToWeb = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",youtubeId]];
        
        if ([[UIApplication sharedApplication] canOpenURL:linkToApp]) {
            // Can open the youtube app URL so launch the youTube app with this URL
            [[UIApplication sharedApplication] openURL:linkToApp];
        }
        else{
            // Can't open the youtube app URL so launch Safari instead
            [[UIApplication sharedApplication] openURL:linkToWeb];
        }
    }
}

-(IBAction)GoBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
