//
//  DetailsMovieViewController.h
//  PrivaliaCaseObjC
//
//  Created by alexei on 4/24/18.
//  Copyright © 2018 privalia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsMovieViewController : UIViewController
@property (nonatomic) NSDictionary *movieDetails;
@end
